package com.example.marcelo.projetorca.modelo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by marcelo on 23/03/18.
 */

public class Installment {

    @SerializedName("past_due")
    @Expose
    private String past_due;

    @SerializedName("carnet")
    @Expose
    private String carnet;

    @SerializedName("value")
    @Expose
    private String value;

    @SerializedName("installment")
    @Expose
    private String installment;

    @SerializedName("detail")
    @Expose
    private Detail detail;

    public Installment() {

    }

    public Installment(String past_due, String carnet, String value, String installment, Detail detail) {
        this.past_due= past_due;
        this.carnet = carnet;
        this.value = value;
        this.installment = installment;
        this.detail = detail;
    }

    public String getPast_due() {
        return past_due;
    }

    public void setPast_due(String past_due) {
        this.past_due = past_due;
    }

    public String getCarnet() {
        return carnet;
    }

    public void setCarnet(String carnet) {
        this.carnet = carnet;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getInstallment() {
        return installment;
    }

    public void setInstallment(String installment) {
        this.installment = installment;
    }

    public Detail getDetail() {
        return detail;
    }

    public void setDetail(Detail detail) {
        this.detail = detail;
    }
}
