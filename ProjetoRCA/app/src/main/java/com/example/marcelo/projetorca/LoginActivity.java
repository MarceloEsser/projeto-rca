package com.example.marcelo.projetorca;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.marcelo.projetorca.API.APIService;
import com.example.marcelo.projetorca.modelo.Installment;
import com.example.marcelo.projetorca.modelo.User;
import com.example.marcelo.projetorca.util.InstallmentAdapter;
import com.example.marcelo.projetorca.util.UserDeserialize;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {

    public static final String TAG = "11";
    private EditText etCampo1;
    private EditText etCampo2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etCampo1 = findViewById(R.id.login_activity_et1);
        etCampo2 = findViewById(R.id.login_activity_et2);

        Button btnLogin = findViewById(R.id.login_activity_btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etCampo1.getText().toString().equals(etCampo2.getText().toString())) {
                    pegaUsers(etCampo1.getText().toString());
                } else {
                    AlertDialog.Builder msg = new AlertDialog.Builder(LoginActivity.this);
                    msg.setTitle("Erro");
                    msg.setMessage("Senhas não conferem");
                    msg.setPositiveButton("Ok", null);
                    msg.show();
                }
            }
        });
    }


    public void pegaUsers(String user) {
        Gson gson = new GsonBuilder().registerTypeAdapter(User.class, new UserDeserialize()).create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIService.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        APIService service = retrofit.create(APIService.class);

        Call<User> callUser = service.getUser(user);

        callUser.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {

                if (!response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "User não registado no banco de dados!", Toast.LENGTH_LONG).show();
                } else {
                    if (response.body().getData() == null){
                        Toast.makeText(getApplicationContext(), "User não registado no banco de dados!", Toast.LENGTH_LONG).show();
                    }
                    else {
                        Intent itVaiExtratos = new Intent(LoginActivity.this, ExtratosActivity.class);
                        itVaiExtratos.putExtra("senha", etCampo1.getText().toString());
                        startActivity(itVaiExtratos);
                    }
                }
            }
            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Error: " + t.getMessage(), Toast.LENGTH_LONG).show();
                Log.e(TAG, "Error: " + t.getMessage());
            }
        });


    }

}
