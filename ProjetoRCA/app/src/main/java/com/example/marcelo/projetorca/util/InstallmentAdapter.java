package com.example.marcelo.projetorca.util;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.hardware.camera2.params.BlackLevelPattern;
import android.support.v7.app.AlertDialog;
import android.text.style.BackgroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.marcelo.projetorca.ExtratosActivity;
import com.example.marcelo.projetorca.R;
import com.example.marcelo.projetorca.modelo.Installment;

import java.util.List;

/**
 * Created by marcelo on 23/03/18.
 */

public class InstallmentAdapter extends BaseAdapter{

    private Context context;
    private List<Installment> installments;


    public InstallmentAdapter(Context context, List<Installment> installments) {
        this.context = context;
        this.installments = installments;

    }

    @Override
    public int getCount() {
        return installments.size();
    }

    @Override
    public Object getItem(int position) {
        return installments.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final Installment installment = installments.get(position);

        if (convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.list_item, null);
        }

        final TextView tvPastDue = convertView.findViewById(R.id.list_item_tv_past_due);
        TextView tvCarnet = convertView.findViewById(R.id.list_item_tv_carnet);
        TextView tvInstallment = convertView.findViewById(R.id.list_item_tv_installment);
        TextView tvValue = convertView.findViewById(R.id.list_item_tv_value);

        tvPastDue.setText(installment.getPast_due());
        tvCarnet.setText(installment.getCarnet());
        tvInstallment.setText(installment.getInstallment());
        tvValue.setText(installment.getValue());

        return convertView;

    }



}
