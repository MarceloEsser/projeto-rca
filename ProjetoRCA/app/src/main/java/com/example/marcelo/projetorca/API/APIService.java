package com.example.marcelo.projetorca.API;

import com.example.marcelo.projetorca.modelo.User;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by marcelo on 23/03/18.
 */

public interface APIService {

    String BASE_URL = "http://www.icoded.com.br/api/";
    @GET("extract.php")
    Call<User> getUser(@Query("pwd") String pwd);



}
