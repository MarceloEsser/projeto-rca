package com.example.marcelo.projetorca;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.constraint.solver.widgets.WidgetContainer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.marcelo.projetorca.API.APIService;
import com.example.marcelo.projetorca.modelo.Installment;
import com.example.marcelo.projetorca.modelo.Limits;
import com.example.marcelo.projetorca.modelo.User;
import com.example.marcelo.projetorca.util.InstallmentAdapter;
import com.example.marcelo.projetorca.util.UserDeserialize;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ExtratosActivity extends AppCompatActivity {
    
    public static final String TAG = "10";
    private InstallmentAdapter adapter;

    private ListView lvParcelas;
    private TextView tvValorDisponivel;
    private TextView tvValorLimite;
    private TextView tvValorUtilizado;
    private TextView tvNome;
    private TextView etCampo1;

    private User u;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_extratos);
        
        tvNome = findViewById(R.id.extratos_activity_tv_nome);
        tvValorUtilizado = findViewById(R.id.extratos_activity_tv_valorUtilizado);
        tvValorLimite = findViewById(R.id.extratos_activity_tv_valorLimite);
        tvValorDisponivel = findViewById(R.id.extratos_activity_tv_valorDisponivel);
        lvParcelas = findViewById(R.id.extratos_activity_lv_parcelas);
        etCampo1 = findViewById(R.id.login_activity_et1);



        pegaUsers(getIntent().getStringExtra("senha"));

    }

    public void pegaUsers(final String user) {
        Gson gson = new GsonBuilder().registerTypeAdapter(User.class, new UserDeserialize()).create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIService.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        APIService service = retrofit.create(APIService.class);

        Call<User> callUser = service.getUser(user);

        callUser.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {

                if (!response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "User não registado no banco de dados!", Toast.LENGTH_LONG).show();
                } else {
                    u = response.body();
                    final List<Installment> installments = u.getData().getInstallments();
                    adapter = new InstallmentAdapter(ExtratosActivity.this, installments);
                    lvParcelas.setAdapter(adapter);

                    tvNome.setText(u.getData().getName());
                    tvValorDisponivel.setText(u.getData().getLimits().getAvailable());
                    tvValorLimite.setText(u.getData().getLimits().getTotal());
                    tvValorUtilizado.setText(u.getData().getLimits().getExpent());

                    lvParcelas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                            lvParcelas.getChildAt(position).setBackgroundColor(getResources().getColor(R.color.grey500));

                            AlertDialog.Builder msg = new AlertDialog.Builder(ExtratosActivity.this);
                            msg.setTitle("Data: ");
                            msg.setMessage(installments.get(position).getDetail().toString());
                            msg.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    lvParcelas.getChildAt(position).setBackgroundColor(getResources().getColor(R.color.white));

                                }
                            });
                            msg.show();
                        }
                    });
                }



            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Error: " + t.getMessage(), Toast.LENGTH_LONG).show();
                Log.e(TAG, "Error: " + t.getMessage());
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.extratos_activity, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_extratos_voltar:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}